package co.spribe.game.model;

public interface Player {
    String getName();

    HandShape play();
}
