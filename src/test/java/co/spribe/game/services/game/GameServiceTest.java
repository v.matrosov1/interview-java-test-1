package co.spribe.game.services.game;

import co.spribe.game.model.HandShape;
import co.spribe.game.model.Player;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static utils.PlayerMockHelper.createPlayerWithTargetShape;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class GameServiceTest {

    @InjectMocks
    GameServiceImpl gameService;

    @Test
    void playGame_whenOnePlayerHasRockAndSecondPaper_playerWithPaperWin() {
        Player player1 = createPlayerWithTargetShape(HandShape.ROCK, "player1");
        Player player2 = createPlayerWithTargetShape(HandShape.PAPER, "player2");

        GameResult gameResult = gameService.playGame(player1, player2);

        assertNotNull(gameResult);
        assertEquals(gameResult.getWinner(), player2);
        assertEquals(gameResult.getWinner().getName(), player2.getName());
        assertTrue(gameResult.getAllPlayers().contains(player1));
        assertTrue(gameResult.getAllPlayers().contains(player2));
    }

    @Test
    void playGame_whenOnePlayerHasRockAndSecondScissors_playerWithRockWin() {
        Player player1 = createPlayerWithTargetShape(HandShape.ROCK, "player1");
        Player player2 = createPlayerWithTargetShape(HandShape.SCISSORS, "player2");

        GameResult gameResult = gameService.playGame(player1, player2);

        assertNotNull(gameResult);
        assertEquals(gameResult.getWinner(), player1);
        assertEquals(gameResult.getWinner().getName(), player1.getName());
        assertTrue(gameResult.getAllPlayers().contains(player1));
        assertTrue(gameResult.getAllPlayers().contains(player2));
    }

    @Test
    void playGame_whenOnePlayerHasPaperAndSecondScissors_playerWithScissorsWin() {
        Player player1 = createPlayerWithTargetShape(HandShape.ROCK, "player1");
        Player player2 = createPlayerWithTargetShape(HandShape.SCISSORS, "player2");

        GameResult gameResult = gameService.playGame(player1, player2);

        assertNotNull(gameResult);
        assertEquals(gameResult.getWinner(), player2);
        assertEquals(gameResult.getWinner().getName(), player2.getName());
        assertTrue(gameResult.getAllPlayers().contains(player1));
        assertTrue(gameResult.getAllPlayers().contains(player2));
    }
}