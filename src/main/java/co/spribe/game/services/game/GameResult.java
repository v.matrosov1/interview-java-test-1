package co.spribe.game.services.game;

import co.spribe.game.model.Player;

import java.util.Set;

public class GameResult {
    private Player winner;
    private Set<Player> allPlayers;

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public Set<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(Set<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }
}
