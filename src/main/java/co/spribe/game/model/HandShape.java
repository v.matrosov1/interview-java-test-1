package co.spribe.game.model;

public enum HandShape {
    ROCK, PAPER, SCISSORS
}
