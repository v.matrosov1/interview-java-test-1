package co.spribe.game.services.game;

import co.spribe.game.exception.GameException;
import co.spribe.game.model.Player;

public interface GameService {
    GameResult playGame(Player... players) throws GameException;
}
