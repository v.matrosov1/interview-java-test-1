package utils;

import co.spribe.game.model.HandShape;
import co.spribe.game.model.Player;
import org.mockito.Mockito;

public class PlayerMockHelper {
    public static Player createPlayerWithTargetShape(HandShape handShape, String name) {
        Player player = Mockito.mock(Player.class);

        Mockito.doReturn(handShape)
                .when(player).play();

        Mockito.doReturn(name)
                .when(player).getName();

        return player;
    }
}
